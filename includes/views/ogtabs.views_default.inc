<?php

/**
 *  Implements hook_views_default_views().
 */
function og_tabs_views_default_views() {
  // Begin copy and paste of output from the Export tab of a view.
$view = new view();
$view->name = 'group_full_calendars';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Group Full Calendars';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Group Full Calendars';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'fullcalendar';
/* Relationship: OG membership: OG membership from Node */
$handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
$handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
$handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Start and End Dates */
$handler->display->display_options['fields']['field_syllabi_start_date']['id'] = 'field_syllabi_start_date';
$handler->display->display_options['fields']['field_syllabi_start_date']['table'] = 'field_data_field_syllabi_start_date';
$handler->display->display_options['fields']['field_syllabi_start_date']['field'] = 'field_syllabi_start_date';
/* Field: Content: Start and End Dates */
$handler->display->display_options['fields']['field_events_start_date']['id'] = 'field_events_start_date';
$handler->display->display_options['fields']['field_events_start_date']['table'] = 'field_data_field_events_start_date';
$handler->display->display_options['fields']['field_events_start_date']['field'] = 'field_events_start_date';
$handler->display->display_options['fields']['field_events_start_date']['label'] = '';
$handler->display->display_options['fields']['field_events_start_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_events_start_date']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
/* Contextual filter: OG membership: Group ID */
$handler->display->display_options['arguments']['gid']['id'] = 'gid';
$handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
$handler->display->display_options['arguments']['gid']['field'] = 'gid';
$handler->display->display_options['arguments']['gid']['default_action'] = 'default';
$handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
$handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'events' => 'events',
  'syllabi' => 'syllabi',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['enabled'] = FALSE;
$handler->display->display_options['path'] = 'node/%/group-full-calendars';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Calendar';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_1');

/* Display: Feed */
$handler = $view->new_display('feed', 'Feed', 'feed_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'date_ical';
$handler->display->display_options['style_options']['cal_name'] = '';
$handler->display->display_options['style_options']['no_calname'] = 0;
$handler->display->display_options['style_options']['disable_webcal'] = 0;
$handler->display->display_options['row_plugin'] = 'date_ical';
$handler->display->display_options['row_options']['date_field'] = 'field_data_field_syllabi_start_date.field_syllabi_start_date_value';
$handler->display->display_options['row_options']['summary_field'] = 'default_title';
$handler->display->display_options['row_options']['location_field'] = 'none';
$handler->display->display_options['path'] = 'node/%/group-full-calendars_export.ics';
$handler->display->display_options['displays'] = array(
  'default' => 'default',
  'page' => 'page',
  'block_1' => 'block_1',
);
    
  // (Export ends here.)

  // Add view to list of views to provide.
  $views[$view->name] = $view;

  // ...Repeat all of the above for each view the module should provide.

  // At the end, return array of default views.
  return $views;
}
