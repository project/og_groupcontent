<?php

/**
 * @file
 * Google Drive api connection
 * Settings form for ID and Secrets
 */

/**
 * Uses the form api for an admin settings form.
 */
function og_groupcontent_settings_form() {
  global $user;
  $og_groupcontent_announcements = variable_get('og_groupcontent_announcements', '');
  $og_groupcontent_blogs = variable_get('og_groupcontent_blogs', '');
  $og_groupcontent_wikis = variable_get('og_groupcontent_wikis', '');
  $og_groupcontent_polls = variable_get('og_groupcontent_polls', '');
  $og_groupcontent_events = variable_get('og_groupcontent_events', '');
  $og_groupcontent_syllabi = variable_get('og_groupcontent_syllabi', '');

  $options = array(
    'announcement' => t('announcements'),
    'blogs' => t('blogs (this blog type is ideal for schools / teaching environments)'),
    'blogs2' => t('blogs (plain old regular blogs)'),
    'wiki' => t('wikis'),
    'poll' => t('polls'),
    'events' => t('events'),
    'syllabi' => t('syllabi'),
  );
  $default_value = variable_get('og_groupcontent_publishable', '');
  $form['container']['og_groupcontent_publishable'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow users to post:'),
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'og_groupcontent_settings_form_submit';
  return $form;
}

/**
 * Form submission handler og_groupcontent_settings_form().
 */
function og_groupcontent_settings_form_submit($form, &$form_state) {

}
