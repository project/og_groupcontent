INTRODUCTION
------------
Welcome to the OG Group Content module. OG Group Content adds content integrated
into organic groups such as blogs, syllabi, wikis, and group announcements.

REQUIREMENTS
------------
og, og_context, og_access, poll, views, libraries, calendar, fullcalendar,
date_ical, views_accordion

RECOMMENDED MODULES
-------------------
* Notifications
  When enabled, group members can recieve emails when new announcements are added.

INSTALLATION
------------
1.)  Enable the og_groupcontent module.
2.)  Select at least one content type that users are allowed to post. admin/config/group/og_groupcontent/settings
3.)  Go and make sure a group content type exists called "group" (or whatever name really should work) and within this content type make sure it's set to be a "Group" (an organic group).
4.)  Make sure all the other other content types that are going to be pieces of content in the group are set as "Group content" and then under "target bundles" choose the group content type that they belong to.  This is all done within the admin/structure/types area where you assign content types to groups and create base group content types.
5.)  Enable the "OG Group Content Create Selective Types of Group Content" block and put it on the pages you would like it to appear on, I suggest just having it on the organic group home pages. (use the Drupal blocks system to do this)
6.)  If using syllabi for administrators only enable the "Syllabus Draggable Edit Form" block only on the node/*/syllabi_list pages within the Drupal blocks system.
7.)  To put announcements on your groups' home page put the announcements block in the content region of your group pages.
8.)  If the notifications module is installed and a user is subscribed to group content or to announcements, when an announcement is created, any group member subscribed will receive announcement emails.

CONFIGURATION
-------------
 * Configure available group content types in Administration » Configuration » Organic groups >> OG Group Content Settings: admin/config/group/og_groupcontent/settings

MAINTAINERS
-----------
Author:
 * James Barnett (barnettech) - https://drupal.org/user/669922
Current maintainers:
 * James Barnett (barnettech) - https://drupal.org/user/669922
 * Jamie Fallon (lokapujya) - https://drupal.org/user/98799

