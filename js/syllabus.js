(function($) {
    $(document).ready(function() {
              $('.node-syllabi').live("click", function(e) {
                e.preventDefault();
                $(this).find('.field-name-body').toggle();
                $(this).find('.submitted').hide();
                $(this).find('.submitted').toggle();
                if ($(this).closest('.syllabus-entry').hasClass('contracted')) {
                  $(this).closest('.syllabus-entry').removeClass('contracted');
                }
                else {
                  $(this).closest('.syllabus-entry').addClass('contracted');
                }
              });
            });
              
}(jQuery));

